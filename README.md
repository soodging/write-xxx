## 介绍

本仓库用于手写一些简易版的框架，诸如 Spring IoC、Junit、Mybatis等，旨在了解框架背后蕴含的设计思想，方便更深一步了解框架底层源码。

## 1、Spring IoC

![手写spring-ioc](README.assets/手写spring-ioc.png)

## 2、Junit

## 3、Bean 生命周期