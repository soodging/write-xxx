package com.xing.service.impl;

import com.xing.dao.UserDao;
import com.xing.service.UserService;

public class UserServiceImpl implements UserService {

    public UserServiceImpl() {
        System.out.println("UserServiceImpl创建了...");
    }

    private UserDao userDao;

    public void setUserDao(UserDao userDao) {
        this.userDao = userDao;
    }

    @Override
    public void add() {
        System.out.println("userServiceImpl ...");
        userDao.add();
    }
}