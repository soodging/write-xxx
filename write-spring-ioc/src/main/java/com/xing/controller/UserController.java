package com.xing.controller;

import com.xing.ioc.context.ApplicationContext;
import com.xing.ioc.context.impl.ClassPathXmlApplicationContext;
import com.xing.service.UserService;

public class UserController {
    public static void main(String[] args) throws Exception {
        //创建spring容器对象
        ApplicationContext applicationContext = new ClassPathXmlApplicationContext("application.xml");
        //从IOC容器中获取UserService对象
        UserService userService = applicationContext.getBean("userService", UserService.class);
        //调用UserService对象的add方法
        userService.add();
    }
}