package com.xing.ioc.tag;

/**
 * 对应配置文件中 Bean 标签
 * @author 17916
 */
public class BeanDefinition {
    private String id;
    private String className;

    /**
     * bean 标签有不止一个 Property 子标签
     */
    private MutablePropertyValues propertyValues;

    public BeanDefinition() {
        propertyValues = new MutablePropertyValues();
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getClassName() {
        return className;
    }

    public void setClassName(String className) {
        this.className = className;
    }

    public void setPropertyValues(MutablePropertyValues propertyValues) {
        this.propertyValues = propertyValues;
    }

    public MutablePropertyValues getPropertyValues() {
        return propertyValues;
    }
}