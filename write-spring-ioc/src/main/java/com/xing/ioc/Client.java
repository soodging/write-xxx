package com.xing.ioc;

import com.xing.ioc.parser.impl.XmlBeanDefinitionReader;
import com.xing.ioc.registry.BeanDefinitionRegistry;

/**
 * [测试 XmlBeanDefinitionReader ]
 *
 * @author : [xing]
 * @version : [v1.0]
 * @createTime : [2023/5/4 14:42]
 */

public class Client {
    public static void main(String[] args) throws Exception {
        XmlBeanDefinitionReader reader = new XmlBeanDefinitionReader();
        reader.loadBeanDefinitions("application.xml");
        BeanDefinitionRegistry registry = reader.getRegistry();
        for (String beanDefinitionName : registry.getBeanDefinitionNames()) {
            System.out.println(beanDefinitionName);
        }

    }
}
