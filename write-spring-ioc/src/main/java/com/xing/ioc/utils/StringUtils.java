package com.xing.ioc.utils;

/**
 * @author 17916
 */
public class StringUtils {
    private StringUtils() {
    }

    /**
     * 用来根据 className 拼接 set方法名，进而进行依赖注入
     * userDao   ==>   setUserDao
     */
    public static String getSetterMethodNameByFieldName(String fieldName) {
        String methodName = "set" + fieldName.substring(0,1).toUpperCase() + fieldName.substring(1);
        return methodName;
    }
}
