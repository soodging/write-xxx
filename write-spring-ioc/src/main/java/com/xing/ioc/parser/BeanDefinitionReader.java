package com.xing.ioc.parser;

import com.xing.ioc.registry.BeanDefinitionRegistry;

/**
 * 解析配置文件，注册bean信息
 * @author 17916
 */
public interface BeanDefinitionReader {


    /**
     * 获取注册表
     * @return BeanDefinitionRegistry
     */
    BeanDefinitionRegistry getRegistry();

    /**
     * 根据路径加载配置文件并在注册表中进行登记
     * @param configLocation
     * @throws Exception
     */
    void loadBeanDefinitions(String configLocation) throws Exception;
}