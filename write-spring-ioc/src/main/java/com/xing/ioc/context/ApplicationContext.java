package com.xing.ioc.context;

/**
 * 该接口子实现类对bean创建都是非延时的
 *
 * @author 17916
 */
public interface ApplicationContext extends BeanFactory {
	/**
     * 进行配置文件加载并进行对象创建
     */
    void refresh() throws IllegalStateException, Exception;
}