package com.xing.ioc.context.impl;

import com.xing.ioc.context.ApplicationContext;
import com.xing.ioc.parser.BeanDefinitionReader;
import com.xing.ioc.registry.BeanDefinitionRegistry;
import com.xing.ioc.tag.BeanDefinition;

import java.util.HashMap;
import java.util.Map;

/**
 * ApplicationContext 的子实现类
 * 非延时加载
 * @author 17916
 */
public abstract class AbstractApplicationContext implements ApplicationContext {

    /**
     * 解析器用来进行xml配置文件的解析，符合单一职责原则
     * BeanDefinitionReader类型的对象创建交由子类实现，因为只有子类明确到底创建BeanDefinitionReader哪儿个子实现类对象。
     */
    protected BeanDefinitionReader beanDefinitionReader;
    /**
     * 用来存储bean对象的容器
     * key：bean的id值
     * value：bean对象
     */
    protected Map<String, Object> singletonObjects = new HashMap<String, Object>();

    //存储配置文件的路径
    protected String configLocation;

    @Override
    public void refresh() throws IllegalStateException, Exception {

        //加载BeanDefinition
        beanDefinitionReader.loadBeanDefinitions(configLocation);

        //初始化bean
        finishBeanInitialization();
    }

    /**
     * bean的初始化
     * finishBeanInitialization()方法中调用getBean()方法使用到了模板方法模式。
     */
    private void finishBeanInitialization() throws Exception {
        BeanDefinitionRegistry registry = beanDefinitionReader.getRegistry();
        String[] beanNames = registry.getBeanDefinitionNames();

        for (String beanName : beanNames) {
            BeanDefinition beanDefinition = registry.getBeanDefinition(beanName);
            getBean(beanName);
        }
    }
}