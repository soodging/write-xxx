package com.xing.ioc.registry;

import com.xing.ioc.tag.BeanDefinition;

/**
 * 规定 BeanDefinition 注册表 应该具有的功能
 * @author 17916
 */
public interface BeanDefinitionRegistry {

    /**
     * 将 beanName 和 beanDefinition 登记到注册表
     * @param beanName
     * @param beanDefinition
     */
    void registerBeanDefinition(String beanName, BeanDefinition beanDefinition);

    /**
     * 从注册表中删除指定名称的BeanDefinition对象
     * @param beanName
     * @throws Exception
     */
    void removeBeanDefinition(String beanName) throws Exception;

    /**
     * 根据名称从注册表中获取BeanDefinition对象
     * @param beanName
     * @return BeanDefinition
     * @throws Exception
     */
    BeanDefinition getBeanDefinition(String beanName) throws Exception;

    boolean containsBeanDefinition(String beanName);

    int getBeanDefinitionCount();

    String[] getBeanDefinitionNames();
}