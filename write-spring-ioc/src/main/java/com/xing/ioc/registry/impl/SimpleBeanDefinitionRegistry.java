package com.xing.ioc.registry.impl;

import com.xing.ioc.registry.BeanDefinitionRegistry;
import com.xing.ioc.tag.BeanDefinition;

import java.util.HashMap;
import java.util.Map;

/**
 * BeanDefinitionRegistry 的简单实现类
 * @author 17916
 */
public class SimpleBeanDefinitionRegistry implements BeanDefinitionRegistry {

    /**
     * key：beanName
     * value：BeanDefinition
     */
    private Map<String, BeanDefinition> beanDefinitionMap = new HashMap<String, BeanDefinition>();

    public Map<String, BeanDefinition> getBeanDefinitionMap() {
        return beanDefinitionMap;
    }

    @Override
    public void registerBeanDefinition(String beanName, BeanDefinition beanDefinition) {
        beanDefinitionMap.put(beanName,beanDefinition);
    }

    @Override
    public void removeBeanDefinition(String beanName) throws Exception {
        beanDefinitionMap.remove(beanName);
    }

    @Override
    public BeanDefinition getBeanDefinition(String beanName) throws Exception {
        return beanDefinitionMap.get(beanName);
    }

    @Override
    public boolean containsBeanDefinition(String beanName) {
        return beanDefinitionMap.containsKey(beanName);
    }

    @Override
    public int getBeanDefinitionCount() {
        return beanDefinitionMap.size();
    }

    @Override
    public String[] getBeanDefinitionNames() {
        return beanDefinitionMap.keySet().toArray(new String[1]);
    }
}