package com.xing.proxy.cglib;

import net.sf.cglib.proxy.Enhancer;
import net.sf.cglib.proxy.MethodInterceptor;
import net.sf.cglib.proxy.MethodProxy;

import java.lang.reflect.Method;

/**
 * [CjlibProxy通过继承实现代理]
 *
 * @author : [xing]
 * @version : [v1.0]
 * @createTime : [2023/7/18 22:30]
 */

public class CjlibProxy {
    public static void main(String[] args) {
        Cook cook = new Cook();
        //cglib：通过 Enhancer.create 创建代理对象
        /*Cook proxy = (Cook) Enhancer.create(cook.getClass(), new MethodInterceptor() {
            @Override
            public Object intercept(Object o, Method method, Object[] objects, MethodProxy methodProxy) throws Throwable {
                String result = (String) method.invoke(cook, objects);
                System.out.println("开始派送订单...");
                System.out.println(objects[0] + "订单已到达...");
                return "送餐结束";
            }
        });
        String result = proxy.cook("鱼香肉丝");
        System.out.println(result);*/

        // 上面 Enhancer.create 其实底层就是创建了一个类似于 DeliverMan 的类，继承Cook
        // 类中有一个 MethodInterceptor 静态变量
        // 重写Cook的方法时，都会调用 MethodInterceptor 中的invoke方法
        // 在DeliverMan构造函数中传入该MethodInterceptor，
        // MethodInterceptor是用户根据自己的需要在invoke方法中添加自己的逻辑

        MethodInterceptor interceptor = new MethodInterceptor() {
            @Override
            public Object intercept(Object o, Method method, Object[] objects, MethodProxy methodProxy) throws Throwable {
                String result = (String) method.invoke(cook, objects[0]);
                System.out.println("开始派送订单...");
                System.out.println(objects[0] + "订单已到达...");
                return "送餐结束";
            }
        };
        DeliverMan deliverMan = new DeliverMan(interceptor);
        String result = deliverMan.cook("鱼香肉丝");
        System.out.println(result);

    }
}
