package com.xing.proxy.cglib;

import com.xing.proxy.jdk.TakeOut;
import net.sf.cglib.proxy.MethodInterceptor;

import java.lang.reflect.Method;

/**
 * [一句话描述该类的功能]
 *
 * @author : [xing]
 * @version : [v1.0]
 * @createTime : [2023/7/18 22:05]
 */

public class DeliverMan extends Cook {

    private final MethodInterceptor interceptor;

    public DeliverMan(MethodInterceptor interceptor) {
        this.interceptor = interceptor;
    }

    @Override
    public String cook(String foodName) {
        Method method = null;
        try {
            method = Cook.class.getMethod("cook", String.class);
            String result = (String) interceptor.intercept(this, method, new Object[]{foodName},null );
            return result;
        } catch (Throwable throwable) {
            throwable.printStackTrace();
        }
        return super.cook(foodName);
    }
}
