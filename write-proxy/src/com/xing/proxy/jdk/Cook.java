package com.xing.proxy.jdk;

/**
 * [被代理的类]
 *
 * @author : [xing]
 * @version : [v1.0]
 * @createTime : [2023/7/18 21:42]
 */

public class Cook implements TakeOut{
    /**
     * @param foodName 菜名
     * @return
     */
    @Override
    public String cook(String foodName){
        System.out.println("收到顾客订单: "+ foodName);
        System.out.println("开始制作"+ foodName +"...");
        return foodName +"已完成";
    }
}
