package com.xing.proxy.jdk;

/**
 * [一句话描述该类的功能]
 *
 * @author : [xing]
 * @version : [v1.0]
 * @createTime : [2023/7/18 21:48]
 */
public interface TakeOut {
    /**
     * 做菜
     * @param foodName 菜名
     * @return
     */
    public String cook(String foodName);

}
