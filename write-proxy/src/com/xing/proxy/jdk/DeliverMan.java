package com.xing.proxy.jdk;

import java.lang.reflect.InvocationHandler;
import java.lang.reflect.Method;

/**
 * [一句话描述该类的功能]
 *
 * @author : [xing]
 * @version : [v1.0]
 * @createTime : [2023/7/18 22:05]
 */

public class DeliverMan implements TakeOut{

    private final InvocationHandler handler;

    public DeliverMan(InvocationHandler handler) {
        this.handler = handler;
    }

    @Override
    public String cook(String foodName) {
        Method method = null;
        try {
            method = TakeOut.class.getMethod("cook", String.class);
            String result = (String) handler.invoke(this, method, new Object[]{foodName});
            return result;
        } catch (Throwable throwable) {
            throwable.printStackTrace();
        }
        return null;
    }
}
