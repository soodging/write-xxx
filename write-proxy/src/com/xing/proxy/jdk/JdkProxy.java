package com.xing.proxy.jdk;

import java.lang.reflect.InvocationHandler;
import java.lang.reflect.Method;
import java.lang.reflect.Proxy;

/**
 * [使用jdk动态代理增强cook类，添加派送外卖的逻辑]
 *
 * @author : [xing]
 * @version : [v1.0]
 * @createTime : [2023/7/18 21:51]
 */

public class JdkProxy {
    public static void main(String[] args) {
        Cook cook = new Cook();
        // 使用 Proxy.newProxyInstance 创建代理对象
/*        TakeOut proxy = (TakeOut) Proxy.newProxyInstance(
                cook.getClass().getClassLoader(),
                cook.getClass().getInterfaces(),
                new InvocationHandler() {
                    @Override
                    public Object invoke(Object proxy, Method method, Object[] args) throws Throwable {
                        String result = (String) method.invoke(cook, args[0]);
                        System.out.println(result);
                        //添加派送逻辑
                        System.out.println("开始派送订单...");
                        System.out.println(args[0] + "订单已到达...");
                        return result;
                    }
                });
        proxy.cook("鱼香肉丝");*/


        // 上面的Proxy.newProxyInstance底层其实是动态在内存中产生了一个类似于生成了DeliverMan的类
        // 也实现了TakeOut接口，实现了cook方法，该类中定义了一个InvocationHandler静态变量，在构造函数中传入该变量
        // InvocationHandler是交给用户，根据自己的需要在invoke方法中添加自己的逻辑

        InvocationHandler invocationHandler = new InvocationHandler() {
            @Override
            public Object invoke(Object proxy, Method method, Object[] args) throws Throwable {
                String result = (String) method.invoke(cook, args[0]);
                System.out.println(result);
                //添加派送逻辑
                System.out.println("开始派送订单...");
                System.out.println(args[0] + "订单已到达...");
                return "送餐结束";
            }
        };
        DeliverMan deliverMan = new DeliverMan(invocationHandler);
        String result = deliverMan.cook("鱼香肉丝");
        System.out.println(result);

    }
}
