## write-proxy

> 手写一个代理模式的demo

主要包括三种

- 静态代理（手动维护代理类的代码）

- 动态代理
  - jdk动态代理
  - cjlib动态代理

### jdk动态代理

在内存中动态生成一个代理类，和目标类实现相同的接口，并重写所有的方法，所有方法中调用的都是invocationHandler中的invoke方法，所以我们才要自己定义一个invocationHandler并且在invoke方法中写我们自己的逻辑。

### cjlib动态代理

在内存中动态生成一个代理类，继承目标类，并重写所有的方法，所有方法中调用的都是MethodInterceptor中的intercept方法，所以我们才要自己定义一个MethodInterceptor并且在intercept方法中写我们自己的逻辑。