package com.xing.service;

import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

/**
 * [一句话描述该类的功能]
 *
 * @author : [xing]
 * @version : [v1.0]
 * @createTime : [2023/5/17 10:30]
 */

public class AService {

    @Transactional(propagation = Propagation.REQUIRED)
    public  void insertA(){
        // do something
        insertB();
        // do something
    }

    @Transactional(propagation = Propagation.NEVER)
    public void insertB(){
        System.out.println("insertB");
    }

    public static void main(String[] args) {
        String s = "";
        String[] split = s.split(",");
        for (String s1 : split) {
            System.out.println(s1);
        }
        System.out.println(split);
        System.out.println(s.split(",").length);
    }
}
class Person{
    String name = "no name";
    public Person(String nm){
        name = nm;
    }

    public Person() {
    }
}
class Student extends Person{
    String sid="000";
    public Student(String id) {
        sid = id;
    }
}
interface A{
    public static int a =0;
     default String f(){
        return null;
    }
}
abstract class B{
    static int a=0;

}


