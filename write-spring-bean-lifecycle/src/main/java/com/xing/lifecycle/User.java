package com.xing.lifecycle;

import org.springframework.beans.BeansException;
import org.springframework.beans.factory.*;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import javax.annotation.PreDestroy;

@Component
public class User implements InitializingBean, BeanNameAware, BeanFactoryAware,  ApplicationContextAware {


	private String name;

	public User() {
		System.out.println("构造函数执行了...");
	}


	@Value("张三")
	public void setName(String name) {
		this.name = name;
		System.out.println("属性注入 name");
	}

	@Override
	public void setBeanName(String name) {
		System.out.println("setBeanName()...");
	}

	@Override
	public void setApplicationContext(ApplicationContext applicationContext) throws BeansException {
		System.out.println("setApplicationContext()...");
	}

	@Override
	public void afterPropertiesSet() throws Exception {
		System.out.println("InitializingBean.afterPropertiesSet()...");
	}


	@Override
	public void setBeanFactory(BeanFactory beanFactory) throws BeansException {
		System.out.println("setBeanFactory()...");
	}

	@PostConstruct
	public void init() {
		System.out.println("init()...");
	}

	@PreDestroy
	public void destroy() throws Exception {
		System.out.println("destroy()...");
	}
}
