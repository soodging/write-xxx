package com.xing.config;

import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;

/**
 * [一句话描述该类的功能]
 *
 * @author : [xing]
 * @version : [v1.0]
 * @createTime : [2023/5/10 18:13]
 */
@Configuration
@ComponentScan("com.xing.lifecycle")
public class SpringConfig {
}
