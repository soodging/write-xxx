package com.xing.junit;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;

public class MyClient {
    @MyTest
    public void test1(){
        System.out.println("test1...");
    }
    @MyTest
    public void test2(){
        System.out.println("test2...");
    }

    public void test3(){
        System.out.println("test3...");
    }
    @MyTest
    public void test4(){
        System.out.println("test4...");
    }

    public static void main(String[] args) throws InvocationTargetException, IllegalAccessException {
        MyClient client = new MyClient();
        Class<MyClient> clientClass = MyClient.class;
        for (Method method : clientClass.getDeclaredMethods()) {
            if (method.isAnnotationPresent(MyTest.class)) {
                method.invoke(client);
            }
        }

    }
}
